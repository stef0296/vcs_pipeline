import requests
import os
from bs4 import BeautifulSoup

# URL_TO_MONITOR = "https://eur-lex.europa.eu/legal-content/EN/ALL/?uri=CELEX:52020PC0080" #change this to the URL you want to monitor
URL_TO_MONITOR = "https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A32021R1119" #change this to the URL you want to monitor
FILE_NAME = ''

def process_html(string):
    soup = BeautifulSoup(string, features="lxml")
    # make the html look good
    soup.prettify()
    title = soup.title.string.replace(" ", '')
    global FILE_NAME
    FILE_NAME = f'{title}.html'
    searchContent = soup.find(id="textTabContent")
    string_content = str(searchContent)
    string_content = string_content.replace("\r", "")

    return string_content

def has_content_changed():
    """Returns true if the webpage was changed, otherwise false."""
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36',
    'Pragma': 'no-cache', 'Cache-Control': 'no-cache'}
    response = requests.get(URL_TO_MONITOR, headers=headers)
    
    processed_response_html = process_html(response.text)

    # create the file if it doesn't exist
    if not os.path.exists(FILE_NAME):
        open(FILE_NAME, 'w+').close()
    
    filehandle = open(FILE_NAME, 'r')
    previous_response_html = filehandle.read() 
    filehandle.close()
    
    if previous_response_html == processed_response_html:
        print('no change')
        return False
    else:
        filehandle = open(FILE_NAME, 'w')
        filehandle.write(processed_response_html)
        filehandle.close()
        print('file changed')
        return True

has_content_changed()